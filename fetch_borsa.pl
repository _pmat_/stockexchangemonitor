#!/usr/bin/perl

# fetch_url.pl
# @author Matteo Pasotti <matteo.pasotti@gmail.com>
# @version 2.1

use strict;
use warnings;
use LWP::UserAgent;

my @POSITIONS = (5,8,10,12,14,16);
my @TITOLI = ("Stmicroelectronics");
@TITOLI = sort @TITOLI;
my $PAGE_NUMBER=9;
my %MAP = (	1 => ['A'],
		2 => ['B'],
		3 => ['C'],
		4 => ['D','E','F','G','H'],
		5 => ['I','J','K','L'],
		6 => ['M','N'],
		7 => ['O','P','Q','R'],
		8 => ['S'],
		9 => ['T','U','V','W','X','Y','Z'] );

my $URL = "http://www.borse.it/Generico.php?T=3&P=";
my $ua = LWP::UserAgent->new;
$ua->agent("pfetch/0.1 ");

# Create a request
my ($req,$cachepage,@cachetit,$q);
$cachepage=0;
$q=0;
for my $t (@TITOLI){
	for my $k (keys %MAP){
		for my $x (@{$MAP{$k}}){
			#print substr($t,0,1)." ".$x." ".$k."\n";
			if($x eq substr($t,0,1)){
				if($cachepage == $k){
					next;
				}else{
					$cachepage=$k;
				}
				$req = HTTP::Request->new(GET => "${URL}${k}");
				$req->content_type('text/plain');
				# Pass request to the user agent and get a response back
				my $res = $ua->request($req);
				
				# Check the outcome of the response
				my @content;
				if ($res->is_success) {
					@content = split(/\n/,$res->content);
					parseTable(@content);
					#print $res->content;
				}else {
					print $res->status_line, "\n";
				}	
			}
		}
	}
}

sub RemoveTag{
	my($text)=@_;
	chomp $text;
	$text=~s/(^\s+|\s+$|span|nobr|td|tr|table|class|=|\"b\"|<|>|\/)//g;
	$text=~s/^\s+|\s+$//g;
	return $text;
}
sub parseTable{
	my (@content)=@_;
	my ($PrezRif,$VarPerc,$PrezUff,$PrezMax,$PrezMin);
	my $i=0;
	for my $line(@content){
		chomp $line;
		for my $t(@TITOLI){
			if(index($line,"${t}")!=-1){
				chomp $line;
				$line=RemoveTag($line);
				print $line."\n";
				$PrezRif=@content[$i+$POSITIONS[0]];
				$PrezRif=RemoveTag($PrezRif);
				print $PrezRif."\n";
				$VarPerc=@content[$i+$POSITIONS[1]];
				$VarPerc=RemoveTag($VarPerc);
				print $VarPerc."\n";
				$PrezUff=@content[$i+$POSITIONS[2]];
				$PrezUff=RemoveTag($PrezUff);
				print $PrezUff."\n";
				$PrezMax=@content[$i+$POSITIONS[3]];
				$PrezMax=RemoveTag($PrezMax);
				print $PrezMax."\n";
				$PrezMin=@content[$i+$POSITIONS[4]];
				$PrezMin=RemoveTag($PrezMin);
				print $PrezMin."\n";
				print "----------\n";
			}
		}
		$i++;
	}
}
